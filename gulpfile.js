//let replace = require('gulp-replace'); //.pipe(replace('bar', 'foo'))
let { src, dest } = require("gulp");
let gulp = require("gulp");
let browsersync = require("browser-sync").create();
let autoprefixer = require("gulp-autoprefixer");
let scss = require("gulp-sass");
let group_media = require("gulp-group-css-media-queries");
let plumber = require("gulp-plumber");
let del = require("del");
let imagemin = require("gulp-imagemin");
let uglify = require("gulp-uglify-es").default;
let rename = require("gulp-rename");
let fileinclude = require("gulp-file-include");
let clean_css = require("gulp-clean-css");
let responsive = require('gulp-responsive');
let fonter = require('gulp-fonter');

let project_name = require("path").basename(__dirname);

let path = {
	build: {
		html: project_name + "/",
		js: project_name + "/js/",
		css: project_name + "/css/",
		images: project_name + "/img/",
		fonts: project_name + "/fonts/"
	},
	src: {
		html: ["src/*.html", "!src/_*.html"],
		js: ["src/js/app.js", "src/js/vendors.js"],
		css: "src/scss/style.scss",
		images: "src/img/**/*.{jpg,png,svg,gif,ico,webp}",
		iconfonts: "src/iconfont/*.svg",
		fonts: "src/fonts/*.{ttf,otf}"
	},
	watch: {
		html: "src/**/*.html",
		js: "src/**/*.js",
		css: "src/scss/**/*.scss",
		images: "src/img/**/*.{jpg,png,svg,gif,ico,webp}",
		iconfonts: "src/iconfont/**/*.svg",
		fonts: "src/fonts/**/*.*"
	},
	clean: "./" + project_name + "/"
};
function browserSync(done) {
	browsersync.init({
		server: {
			baseDir: "./" + project_name + "/"
		},
		notify: false,
		port: 3000
	});
}
function browserSyncReload(done) {
	browsersync.reload();
}
function html() {
	return src(path.src.html, { base: "src/" })
		.pipe(plumber())
		.pipe(fileinclude())
		.pipe(dest(path.build.html))
		.pipe(browsersync.stream());
}
function css() {
	return src(path.src.css, { base: "src/scss/" })
		.pipe(plumber())
		.pipe(
			scss({
				outputStyle: "expanded"
			})
		)
		.pipe(group_media())
		.pipe(
			autoprefixer({
				overrideBrowserslist: ["last 5 versions"],
				cascade: true
			})
		)
		.pipe(dest(path.build.css))
		.pipe(clean_css())
		.pipe(
			rename({
				suffix: ".min",
				extname: ".css"
			})
		)
		.pipe(dest(path.build.css))
		.pipe(browsersync.stream());
}
function js() {
	return src(path.src.js, { base: "src/js/" })
		.pipe(plumber())
		.pipe(fileinclude())
		.pipe(gulp.dest(path.build.js))
		.pipe(uglify(/* options */))
		.pipe(
			rename({
				suffix: ".min",
				extname: ".js"
			})
		)
		.pipe(dest(path.build.js))
		.pipe(browsersync.stream());
}
function images() {
	gulp.src('src/img/content/**/*.{jpg,png,svg,gif,ico,webp}')
		.pipe(plumber())
		/*
		.pipe(responsive({
				// Resize all JPG images to three different sizes: 200, 500, and 630 pixels
				'*.*': [
					{
						width: 480,
						rename: { suffix: '_03' }
					},
					{
						width: 750,
						rename: { suffix: '_02' }
					},
					{
						width: 970,
						rename: { suffix: '_01' }
					}
				]
			},
			{
				// Global configuration for all images
				// The output quality for JPEG, WebP and TIFF output formats
				quality: 80,
				// Use progressive (interlace) scan for JPEG and PNG output
				progressive: true,
				// Strip all metadata
				withMetadata: false
			})
		)
		*/
		.pipe(dest('./' + project_name + '/img/content/'));
	return src(path.src.images)
		.pipe(
			imagemin({
				progressive: true,
				svgoPlugins: [{ removeViewBox: false }],
				interlaced: true,
				optimizationLevel: 3 // 0 to 7
			})
		)
		.pipe(dest(path.build.images));
}
function fonts() {
	return src(path.src.fonts)
		.pipe(plumber())
		.pipe(fonter(
			{
				formats: ['woff', 'ttf']
			}
		))
		.pipe(dest(path.build.fonts));
}
function iconfonts() {
	/*
	 return src(path.src.iconfonts, { base: 'src/iconfont' })
		 .pipe(plumber())
		 .pipe(dest(path.build.fonts));
	 */
}
function clean() {
	if (project_name != 'fls_template') {
		//del('./.gitignore');
		//del('./.git/');
	}
	return del(path.clean);
}
function watchFiles() {
	gulp.watch([path.watch.html], html);
	gulp.watch([path.watch.css], css);
	gulp.watch([path.watch.js], js);
	gulp.watch([path.watch.images], images);
	gulp.watch([path.watch.fonts], fonts);
}

let build = gulp.series(clean, gulp.parallel(html, css, js, images, fonts, iconfonts));
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.html = html;
exports.css = css;
exports.js = js;
exports.images = images;
exports.fonts = fonts;
exports.iconfonts = iconfonts;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = watch;