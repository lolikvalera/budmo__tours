let link = document.querySelectorAll('.gotoblock');
if (link) {
	let blocks = [];
	for (let index = 0; index < link.length; index++) {
		let el = link[index];
		let block_name = el.getAttribute('href').replace('#', '');
		blocks.push(block_name);

		el.addEventListener('click', function (e) {
			let target_block_class = el.getAttribute('href').replace('#', '');
			let target_block = document.querySelector('.' + target_block_class).offsetTop;
			let header_height = document.querySelector('header.header').offsetHeight;
			let target_block_position = target_block - header_height;

			if (document.querySelector('.menu__body.active')) {
				document.querySelector('.menu__body').classList.remove('active');
				document.querySelector('.icon-menu').classList.remove('active');
				document.querySelector('body').classList.remove('lock');
			}

			// ScrollTo
			//window.scrollToDuration(target_block_position, 1000);

			window.scrollTo({
				top: target_block_position,
				behavior: 'smooth'
			});

			e.preventDefault();
		})
	}

	window.addEventListener('scroll', function (el) {
		let old_current_link = document.querySelector('.gotoblock.active');
		if (old_current_link) {
			old_current_link.classList.remove('active');
		}
		for (let index = 0; index < blocks.length; index++) {
			let block = blocks[index];
			let block_offset = document.querySelector('.' + block).offsetTop;
			let block_height = document.querySelector('.' + block).offsetHeight;
			if ((pageYOffset > block_offset - window.innerHeight / 3) && pageYOffset < (block_offset + block_height) - window.innerHeight / 3) {
				let current_link = document.querySelector('[href="#' + block + '"]');
				current_link.classList.add('active');
			}
		}
	})
}


function scrollToDuration(element, duration) {
	var e = document.documentElement;
	if (e.scrollTop === 0) {
		var t = e.scrollTop;
		++e.scrollTop;
		e = t + 1 === e.scrollTop-- ? e : document.body;
	}
	scrollToC(e, e.scrollTop, element, duration);
}

// Element to move, element or px from, element or px to, time in ms to animate
function scrollToC(element, from, to, duration) {
	if (duration <= 0) return;
	if (typeof from === "object") from = from.offsetTop;
	if (typeof to === "object") to = to.offsetTop;

	scrollToX(element, from, to, 0, 1 / duration, 20, easeOutCuaic);
}

function scrollToX(element, xFrom, xTo, t01, speed, step, motion) {
	if (t01 < 0 || t01 > 1 || speed <= 0) {
		element.scrollTop = xTo;
		return;
	}
	element.scrollTop = xFrom - (xFrom - xTo) * motion(t01);
	t01 += speed * step;

	setTimeout(function () {
		scrollToX(element, xFrom, xTo, t01, speed, step, motion);
	}, step);
}
function easeOutCuaic(t) {
	t--;
	return t * t * t + 1;
}