// Dynamic Adapt v.1
// HTML data-move="where(uniq class name),position(digi),when(breakpoint)"
// e.x. data-move="item,2,992"
// Andrikanych Yevhen 2020
let move_array = [];
let move_objects = document.querySelectorAll("[data-move]");
if (move_objects.length > 0) {
	for (let index = 0; index < move_objects.length; index++) {
		let el = move_objects[index];
		let data_move = el.getAttribute("data-move");
		if (data_move != "" || data_move != null) {
			el.setAttribute("data-move-index", index);
			move_array[index] = {
				parent: el.parentNode,
				index: index_in_parent(el)
			};
		}
	}
}
function dynamic_adapt() {
	let w = document.querySelector("body").offsetWidth;
	if (move_objects.length > 0) {
		for (let index = 0; index < move_objects.length; index++) {
			let el = move_objects[index];
			let data_move = el.getAttribute("data-move");
			if (data_move != "" || data_move != null) {
				let data_array = data_move.split(",");
				let data_parent = document.querySelector("." + data_array[0]);
				let data_index = data_array[1];
				let data_bp = data_array[2];
				if (w < data_bp) {
					if (!el.classList.contains("js-move_done_" + data_bp)) {
						if (data_index > 0) {
							//insertAfter
							let actual_index = index_of_elements(data_parent)[data_index];
							data_parent.insertBefore(el, data_parent.childNodes[actual_index]);
						} else {
							data_parent.insertBefore(el, data_parent.firstChild);
						}
						el.classList.add("js-move_done_" + data_bp);
					}
				} else {
					if (el.classList.contains("js-move_done_" + data_bp)) {
						dynamic_adaptive_back(el);
						el.classList.remove("js-move_done_" + data_bp);
					}
				}
			}
		}
	}
}
function dynamic_adaptive_back(el) {
	let index_original = el.getAttribute("data-move-index");
	let move_place = move_array[index_original];
	let parent_place = move_place["parent"];
	let index_place = move_place["index"];
	if (index_place > 0) {
		//insertAfter
		let actual_index = index_of_elements(parent_place)[index_place];
		parent_place.insertBefore(el, parent_place.childNodes[actual_index]);
	} else {
		parent_place.insertBefore(el, parent_place.firstChild);
	}
}
function index_in_parent(node) {
	let children = node.parentNode.childNodes;
	let num = 0;
	for (let i = 0; i < children.length; i++) {
		if (children[i] == node) return num;
		if (children[i].nodeType == 1) num++;
	}
	return -1;
}
function index_of_elements(parent) {
	let children = [];
	for (let i = 0; i < parent.childNodes.length; i++) {
		if (parent.childNodes[i].nodeType == 1 && parent.childNodes[i].getAttribute("data-move") == null) {
			children.push(i);
		}
	}
	return children;
}
window.addEventListener("resize", function(event) {
	dynamic_adapt();
});
dynamic_adapt();
